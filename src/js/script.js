$(document).ready(function() {
  svg4everybody();

  // Table tabs
  $('#myTabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  })

  // Smooth scroll
  // $('a[href*="#"]')
  // .not('[href="#"]')
  // .not('[href="#0"]')
  // .click(function(event) {
  //   if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
  //   var target = $(this.hash);
  //   target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
  //     if (target.length) {
  //       event.preventDefault();
  //       $('html, body').animate({
  //         scrollTop: target.offset().top
  //       }, 1000, function () {
  //         var $target = $(target);
  //         $target.attr('tabindex','-1');
  //       });
  //     }
  //   }
  // });

  // Open / Close navigation menu
  $('.s-hamburger').on('click', function () {
    if ( !$('.s-main-nav').hasClass('s-main-nav--is-active') ) {
      $('.s-main-nav').addClass('s-main-nav--is-active');
      $('.s-hamburger').addClass('s-hamburger--is-active');
      $('body').addClass('prevent-scroll');
    } else {
      $('.s-main-nav').removeClass('s-main-nav--is-active');
      $('.s-hamburger').removeClass('s-hamburger--is-active');
      $('body').removeClass('prevent-scroll');
    }
  });

  $('.s-main-nav__list').children().each(function () {
    $(this).children().on('click', function () {
      if ($('.s-main-nav').hasClass('s-main-nav--is-active')) {
        $('.s-main-nav').removeClass('s-main-nav--is-active');
        $('body').removeClass('prevent-scroll');
      }
    });
  });

  $('.s-promo__slider').owlCarousel({
    items: 1,
  });

  $('.s-gallery__slider').owlCarousel({
    margin: 30,
    dots: false,
    stagePadding: 50,
    responsive: {
      0: {
        items: 1,
        nav: false,
      },
      448: {
        items: 2,
        nav: false
      },
      992: {
        items: 3
      }
    }
  });

});